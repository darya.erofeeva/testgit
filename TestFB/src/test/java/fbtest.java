import com.sun.jna.platform.win32.Sspi;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.WebElement;
import org.testng.reporters.jq.TimesPanel;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.support.ui.Wait;
import java.util.List;

public class fbtest {
    public WebDriver driver;


    @BeforeMethod
    void setup() throws MalformedURLException{
        System.setProperty("сhromedriver", "/Users/Dasha/testgit/TestFB/chromedriver");
        driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), DesiredCapabilities.chrome());
    }

    //Login
 @Test
    public void login() {
     driver.get("https://fb.com");
     driver.findElement(By.name("email")).sendKeys("litstest@i.ua");
     driver.findElement(By.id("pass")).sendKeys("Linux-32");
     driver.findElement(By.id("loginbutton")).click();
     driver.manage().window().maximize();
        WebElement title = driver.findElement(By.id("pagelet_welcome"));
        title.getCssValue("Welcome to Facebook, Epistafiy");
        Assert.assertTrue(title.isDisplayed(), "You're not login");
        driver.quit();
    }

 //Log out
@Test
    public void logout() {
    driver.get("https://fb.com");
    driver.findElement(By.name("email")).sendKeys("litstest@i.ua");
    driver.findElement(By.id("pass")).sendKeys("Linux-32");
    driver.findElement(By.id("loginbutton")).click();
    driver.manage().window().maximize();
        WebElement log = driver.findElement(By.id("userNavigationLabel"));
        Actions action = new Actions(driver);
        action.moveToElement(log).click().perform();
    driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.findElement(By.xpath(".//*[@id='BLUE_BAR_ID_DO_NOT_USE']//div[5]/ul/li[12]/a/span[2]"));

    }

    //Find friends
   @Test
  public void friends() {
       driver.get("https://fb.com");
       driver.findElement(By.name("email")).sendKeys("litstest@i.ua");
       driver.findElement(By.id("pass")).sendKeys("Linux-32");
       driver.findElement(By.id("loginbutton")).click();
       driver.manage().window().maximize();
       
        //can't press on button:
       /*WebElement webElem = driver.findElement(By.id("findFriendsNav"));
      Actions action = new Actions(driver);
        action.moveToElement(webElem).click();
       driver.manage().window().maximize();*/


//works in this way:
       driver.get("https://www.facebook.com/?sk=ff");
       WebElement fr = driver.findElement(By.xpath(".//*[@id='fbSearchResultsBox']//ul[1]//li[3]//button[1]"));
       Actions action = new Actions(driver);
       action.moveToElement(fr).click();

  /*  List<WebElement> links = driver.findElements(By.linkText("Add Friend"));
       for (int i = 0; i < links.size(); i++) {
           links = driver.findElements(By.linkText("Add Friend"));
           links.get(2).click();

       }*/
   }
    //Incorrect email
    @Test
    public void emails() {
        driver.get("https://fb.com");
        driver.findElement(By.name("firstname")).sendKeys("qa");
        driver.findElement(By.name("lastname")).sendKeys("qa");
        driver.findElement(By.name("reg_email__")).sendKeys("litstest");
        driver.findElement(By.name("websubmit")).click();
        driver.findElement(By.xpath(".//*[@id='globalContainer']/div[3]/div/div"));

    }
   }



